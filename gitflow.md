# <b> TELKOMDEV GITFLOW </b>

<H3> Branch yang ada di <b> Developer </b> : </H3>

* <b> master </b> 
* <b> develop </b>
* <b> feature </b>
* <b> bugfix </b>

# <H2> TAHAP TAHAP GITFLOW </H2>

* Clone Repo


        <p> $ git clone <SSH/HTTP/HTTPS Repo Kita(origin)> </p>

    ex:

    `$ git clone git@gitlab.com:telkomdev-alipparhan/gitflow.git`


<li> Atur Konfigurasi git </li>

    $ git remote add upstream <SSH/HTTP/HTTPS Repo Upstream>

    ex:
    $ git remote add upstream git@gitlab.com:telkomdev-reza/gitflow.git

    
<li> Buat branch develop </li>

    $ git branch develop


<li> Membuat fitur </li>

Ketika Kita akan membuat sebuah fitur, fitur tersebut kita buatkan branch baru dengan penamaan :

* feature- <nama_fitur>

Sebagai contoh :

    $ git branch feature-login


